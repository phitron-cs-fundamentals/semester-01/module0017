#include <stdio.h>

//string length by recursive function

int fun(char a[], int i)
{
    if (a[i] == '\0')
        return 0; //base case 
    int l = fun(a, i + 1);
    return l + 1;
}

int main()
{
    char a[7] = "hellow"; //initial 
    int length = fun(a, 0);
    printf("%d\n", length);
    return 0;
}